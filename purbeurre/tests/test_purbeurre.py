from django.test import TestCase
from django.test import Client
from django.urls import reverse

client = Client()


class TestPurbeurre(TestCase):
    @classmethod
    def setUpTestData(cls):
        pass

    def test_index_page_name(self):
        # index return 200 with index name
        response = client.get(reverse('index')).status_code
        self.assertEqual(response, 200)

    def test_index_page_url(self):
        # index return 200 with index url
        response = client.get('/').status_code
        self.assertEqual(response, 200)

    def test_legal_page_name(self):
        # legal return 200 with legal name
        response = client.get(reverse('legal')).status_code
        self.assertEqual(response, 200)

    def test_legal_page_url(self):
        # legal return 200 with legal url
        response = client.get('/legal').status_code
        self.assertEqual(response, 200)

    def test_fake_page_url(self):
        # index return 404
        response = client.get('/zzNotExistPaGe').status_code
        self.assertEqual(response, 404)
